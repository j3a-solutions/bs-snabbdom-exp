import commonjs from "rollup-plugin-commonjs";
import resolve from "rollup-plugin-node-resolve";

export default {
  input: "lib/es6/src/demo/snabbdom_demo.js",
  plugins: [resolve(), commonjs()],
  output: {
    file: "dist/main.bundle.js",
    name: "bs_snabbdom",
    format: "iife"
  }
};
