# Changelog

## [Unreleased]

### Changed

- Upgraded all dependencies:

```
 bs-platform              ^1.8.1  →   ^4.0.5
 snabbdom                 ^0.6.9  →   ^0.7.1
 http-server             ^0.10.0  →  ^0.11.1
 rollup                  ^0.43.0  →  ^0.65.0
 rollup-plugin-commonjs   ^8.0.2  →   ^9.1.6
 rollup-plugin-uglify     ^2.0.1  →   ^4.0.0
```

- Moved from npm to yarn
- New package.json scripts
- Upgraded `ml` files to latest bsb APIs
- Upgraded rollup config
- HTML5 standards for demo file
- New folder structure to separate demo from src files
- Removed simple store from src
